package task;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

@Test
public class TruflaTest {
private WebDriver driver;
public void f() throws InterruptedException {
	  
	  System.setProperty("Webdriver.chrome.driver","C:\\webdriverchromedriver.exe");
		WebDriver driver=new ChromeDriver();
		String url="https://play.google.com/store?hl=en";
		String pass="taha123456";
		String mail="taha.kattab99@gmail.com";
		String comment="Test";
		
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElement(By.id("gb_70")).click();//press on sign in   
        driver.findElement(By.id("identifierId")).sendKeys(mail);//enter the register mail 
        driver.findElement(By.className("CwaK9")).click();//press on next button 
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys(pass);//enter the password 
        driver.findElement(By.className("CwaK9")).click();//press on next 
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div[1]/div/ul/li[2]/a/span/span[1]")).click();//press on application
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[1]/c-wiz[1]/ul/li[1]/ul/li[4]/a/span[2]")).click(); //press on game 
        
        driver.findElement(By.xpath("//*[@id=\"body-content\"]/div/div/div[1]/div[2]/div[1]/div/div[2]/div[1]/div/div[1]/a/span[2]")).click();//select the first game 
        Thread.sleep(5000);         
        driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button/span[2]")).click();//add to whishlist
        driver.navigate().back();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"body-content\"]/div/div/div[1]/div[2]/div[1]/div/div[2]/div[9]/div/div[1]/a/span[2]")).click();//select the last game 
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[1]/c-wiz/wishlist-add/button/span[2]")).click();//add to wishlist
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[1]/c-wiz[1]/div[2]/ul/li[4]/a/span[2]")).click();//open my wishlist
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"body-content\"]/div/div/div[1]/div/div/div/div[2]/div[1]/div/div[1]/a")).click();//open the first game to install 
        driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[2]/c-wiz/div/span/button")).click(); //press on install button
        Thread.sleep(10000);
        driver.findElement(By.xpath("Thread.sleep(5000);")).sendKeys(pass);//verify the password
        driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content/span")).click();//press on next button 
        Thread.sleep(5000);
       
        driver.findElement(By.linkText("https://play.google.com/apps")).click();//open my applications 
        Thread.sleep(3000);	
        driver.findElement(By.xpath("//*[@id=\"body-content\"]/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/a/span[2]")).click();//open the installed game form my application 
        WebElement Element = driver.findElement(By.className("LkLjZd ScJHi HPiPcc id-track-click "));
        JavascriptExecutor js = null;//to scroll down in the page until found the write comment option
		js.executeScript("arguments[0].scrollIntoView();", Element);
		driver.findElement(By.id("yDmH0d")).click();//press on write comment button 
		Thread.sleep(3000);
		driver.findElement(By.className("play-button id-enable-gpr apps")).click();
		driver.findElement(By.className("review-input-text-box write-review-comment")).sendKeys(comment);
		driver.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[4]/div/div[2]/div[2]/div[4]/div/div/div[1]/div[3]/button[5]")).click();//rate the game 5
		driver.findElement(By.className("id-submit-review play-button apps")).click();// submit the review and the rate 
		driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/div/c-wiz/div/div[2]/div/div[2]/div[1]/div[2]/div/div[1]/content/span/span")).click();//delete review 
        }
        @AfterTest
        public void closeDriver() {
        driver.quit();//close the driver
        System.out.println("Done");
  }
  
		
}
       

  
